package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        guild.add(this);
        //ToDo: Complete Me
    }
    @Override
    public void update() {
        if (!guild.getQuestType().equals("E"))
            getQuests().add(guild.getQuest());
    }

    //ToDo: Complete Me
}
