package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    @Override
    public String attack() {
        return "Sihir";
    }

    @Override
    public String getType() {
        return "AttackWithMagic";
    }

    //ToDo: Complete me
}
